package com.example.demo;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpContextTest {
    public static void main(String[] args) throws IOException {

        HttpContext httpContext = new BasicHttpContext();
        HttpClientContext httpClientContext = HttpClientContext.adapt(httpContext);

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8080/request");
        // 模仿form表单请求，设置请求参数
        List<NameValuePair> nvp = new ArrayList<>();
        nvp.add(new BasicNameValuePair("username", "admin"));
        // 第一次请求时，设置请求参数
        httpPost.setEntity(new UrlEncodedFormEntity(nvp));

        CloseableHttpResponse response = null;
        try {
            response = httpclient.execute(httpPost, httpClientContext);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String ret = EntityUtils.toString(entity);
                System.out.println("第一次请求响应："+ ret);
            }
        }finally {
            response.close();
        }

        System.out.println("=================第二次请求====================");
        // 重新创建一个HttpPost对象，但是此次该对象中不设置请求参数
        httpPost = new HttpPost("http://localhost:8080/request");
        try {
            response = httpclient.execute(httpPost, httpClientContext);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String ret = EntityUtils.toString(entity);
                System.out.println("第二次请求响应："+ ret);
            }
        }finally {
            if (response!=null)
            response.close();
        }
    }
}
