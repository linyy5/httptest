package com.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@RestController
public class RequestController {
    @GetMapping("/")
    public void index() {
        System.out.println("index");
        return;
    }

    @PostMapping("/request")
    public void request(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //1. 从session中获取username
        String username = (String) session.getAttribute("username");
        String ret;
        if (username == null) {
            // 2. 从请求参数中获取username的值
            username = request.getParameter("username");
            session.setAttribute("username", username);
            ret = "login success!";
        } else {
            ret = "Having been logined :" + username;
        }
        // 将ret 内容写回到response响应体中
        ServletOutputStream outputStream = null;
        PrintWriter pw = null;

        try {
            outputStream = response.getOutputStream();
            pw = new PrintWriter(outputStream);
            pw.write(ret);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
